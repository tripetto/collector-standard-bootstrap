import { IDefinition, ISnapshot, Instance } from "tripetto-collector";
import { Collector } from "./collector";
import { ICollectorStyle } from "./interfaces/style";
import { ICollectorOverrides } from "./interfaces/overrides";
import { ICollectorSnapshot } from "./interfaces/snapshot";
import * as React from "react";
import * as ReactDOM from "react-dom";

export async function run(props: {
    /** Specifies the parent element for the collector. */
    readonly element: HTMLElement | null;

    /** Specifies the definition to run. */
    readonly definition?: IDefinition | Promise<IDefinition>;

    /** Specifies the snapshot that should be restored. */
    readonly snapshot?: ISnapshot<ICollectorSnapshot> | Promise<ISnapshot<ICollectorSnapshot> | undefined>;

    /** Specifies the styles for the collector. */
    readonly style?: ICollectorStyle | Promise<ICollectorStyle | undefined>;

    /** Specifies the overrides for the collector. */
    readonly overrides?: ICollectorOverrides | Promise<ICollectorOverrides>;

    /** Specifies the view mode (`normal` by default). */
    readonly view?: "normal" | "preview" | "test";

    /** Specifies how the collector is used (defaults to `inline`). */
    readonly usage?: "inline" | "frame" | "standalone" | "preview";

    /** Specifies the operation mode (`paginated` by default). */
    readonly mode?: "paginated" | "continuous" | "progressive";

    /** Specifies if the collector needs to be automatically resized when the host window is resized. */
    readonly autoResize?: boolean;

    /** Specifies a React ref for the collector component. */
    readonly ref?: React.RefObject<Collector>;

    /** Specifies a function that is invoked when the collector is rendered. */
    readonly onReady?: (collector: Collector) => void;

    /** Invoked when there is a change. */
    readonly onChange?: (collector: Collector) => void;

    /** Specifies a function that is invoked when the collector is finished. */
    readonly onFinish?: (instance: Instance) => void;

    /**
     * Specifies a function that is invoked when the collector wants to
     * pause (return `true` in the supplied function when you want to wait with
     * the actual pause until the `pause` function is invoked)
     */
    readonly onPause?: (collector: Collector) => boolean | void;

    /** Specifies a function that is invoked when the collector is paused. */
    readonly onPaused?: (snapshot: ISnapshot<ICollectorSnapshot>) => void;

    /** Specifies a function that is invoked when the edit button of a block in preview mode is clicked. */
    readonly onEditRequest?: (nodeId: string) => void;

    /** Specifies attachment related settings. */
    readonly onAttachment?: {
        /** Specifies the function to retrieve an attachment. */
        get: (file: string) => Promise<Blob>;

        /** Specifies the function that is invoked when a file is attached. */
        put: (file: File, onProgress: (percent?: number) => void) => Promise<string>;

        /** Specifies the function that is invoked when an attachment is deleted. */
        delete: (file: string) => Promise<void>;
    };
}): Promise<{
    /** A reference to the collector component. */
    readonly collector: Collector;

    /** Specifies the definition. */
    definition: IDefinition;

    /** Specifies the style. */
    style: ICollectorStyle | undefined;

    /** Specifies the overrides. */
    overrides: ICollectorOverrides | undefined;

    /** Specifies the view mode. */
    view: "normal" | "preview" | "test";

    /** Specifies the operation mode. */
    mode: "paginated" | "continuous" | "progressive";

    /** Retrieves the height of the content in pixels. */
    readonly height: number;

    /** Request a preview of specified node. */
    readonly requestPreview: (nodeId: string) => void;

    /** Requests a pause. */
    readonly requestPause: () => void;

    /** Pauses the collector. */
    readonly pause: () => void;

    /** Indicates the parent element is resized. */
    readonly resize: () => void;

    /** Destroys the component. */
    readonly destroy: () => void;
}> {
    const ref = props.ref || React.createRef<Collector>();
    const emptyDefinition = {
        clusters: [],
        editor: {
            name: "",
            version: ""
        }
    };

    const [definition, snapshot, style, overrides] = await Promise.all([
        props.definition instanceof Promise ? props.definition : Promise.resolve(props.definition),
        props.snapshot instanceof Promise ? props.snapshot : Promise.resolve(props.snapshot),
        props.style instanceof Promise ? props.style : Promise.resolve(props.style),
        props.overrides instanceof Promise ? props.overrides : Promise.resolve(props.overrides)
    ]);

    ReactDOM.render(
        <Collector
            definition={definition || emptyDefinition}
            snapshot={snapshot}
            style={style}
            overrides={overrides}
            view={props.view}
            usage={props.usage}
            mode={props.mode}
            ref={ref}
            onReady={props.onReady}
            onChange={props.onChange}
            onFinish={props.onFinish}
            onPause={props.onPause}
            onPaused={props.onPaused}
            onEditRequest={props.onEditRequest}
            onAttachment={props.onAttachment}
        />,
        props.element
    );

    if (props.autoResize) {
        const fnResize = () => {
            if (ref.current) {
                ref.current.resize();
            }
        };

        window.addEventListener("resize", () => fnResize());
        window.addEventListener("orientationchange", () => fnResize());
    }

    return {
        get collector(): Collector {
            if (!ref.current) {
                throw new Error("The ref to the collector component is not available yet.");
            }

            return ref.current;
        },
        get definition(): IDefinition {
            return (ref.current && ref.current.definition) || definition || emptyDefinition;
        },
        set definition(d: IDefinition) {
            if (ref.current) {
                ref.current.reload(d);
            }
        },
        get style(): ICollectorStyle | undefined {
            if (!ref.current) {
                throw new Error("The ref to the collector component is not available yet.");
            }

            return ref.current.style;
        },
        set style(t: ICollectorStyle | undefined) {
            if (ref.current) {
                ref.current.style = t || {};
            }
        },
        get overrides(): ICollectorOverrides | undefined {
            if (!ref.current) {
                throw new Error("The ref to the collector component is not available yet.");
            }

            return ref.current.overrides;
        },
        set overrides(c: ICollectorOverrides | undefined) {
            if (ref.current) {
                ref.current.overrides = c || {};
            }
        },
        get view(): "normal" | "preview" | "test" {
            return ref.current ? ref.current.view : props.view || "normal";
        },
        set view(view: "normal" | "preview" | "test") {
            if (ref.current) {
                ref.current.view = view;
            }
        },
        get mode(): "paginated" | "continuous" | "progressive" {
            return ref.current ? ref.current.mode : props.mode || "paginated";
        },
        set mode(mode: "paginated" | "continuous" | "progressive") {
            if (ref.current) {
                ref.current.mode = mode;
            }
        },
        get height(): number {
            return (
                (ref.current &&
                    ref.current.element &&
                    ref.current.element.current &&
                    ref.current.element.current.getBoundingClientRect().height) ||
                0
            );
        },
        requestPreview: (nodeId: string) => {
            if (ref.current) {
                ref.current.requestPreview(nodeId);
            }
        },
        requestPause: () => {
            if (ref.current) {
                ref.current.requestPause();
            }
        },
        pause: () => {
            if (ref.current) {
                ref.current.pause();
            }
        },
        resize: () => {
            if (ref.current) {
                ref.current.resize();
            }
        },
        destroy: () => {
            if (props.element) {
                ReactDOM.unmountComponentAtNode(props.element);
            }
        }
    };
}
