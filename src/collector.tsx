import * as React from "react";
import { Debounce, IDefinition, ISnapshot, Instance, castToBoolean } from "tripetto-collector";
import { Blocks } from "./blocks/blocks";
import { ICollectorState } from "./interfaces/state";
import { ICollectorStyle } from "./interfaces/style";
import { ICollectorOverrides } from "./interfaces/overrides";
import { ICollectorSnapshot } from "./interfaces/snapshot";
import { emptyMessage } from "./components/messages/empty";
import { pausedMessage } from "./components/messages/paused";
import { stoppedMessage } from "./components/messages/stopped";
import { finishedMessage } from "./components/messages/finished";
import { pauseDialog } from "./components/pause";
import "bootstrap";
import "./collector.scss";

export class Collector extends React.PureComponent<{
    /** Specifies the definition to run. */
    readonly definition: IDefinition;

    /** Specifies the snapshot that should be restored. */
    readonly snapshot?: ISnapshot<ICollectorSnapshot>;

    /** Specifies the style settings for the collector. */
    readonly style?: ICollectorStyle;

    /** Specifies the overrides for the collector. */
    readonly overrides?: ICollectorOverrides;

    /** Specifies the view mode (`normal` by default). */
    readonly view?: "normal" | "preview" | "test";

    /** Specifies how the collector is used (defaults to `inline`). */
    readonly usage?: "inline" | "frame" | "standalone" | "preview";

    /** Specifies the operation mode (`paginated` by default). */
    readonly mode?: "paginated" | "continuous" | "progressive";

    /** Specifies a function that is invoked when the collector is rendered. */
    readonly onReady?: (collector: Collector) => void;

    /** Invoked when there is a change. */
    readonly onChange?: (collector: Collector) => void;

    /** Specifies a function that is invoked when the collector is finished. */
    readonly onFinish?: (instance: Instance) => void;

    /**
     * Specifies a function that is invoked when the collector wants to
     * pause (return `true` in the supplied function when you want to wait with
     * the actual pause until the `pause` function is invoked)
     */
    readonly onPause?: (collector: Collector) => boolean | void;

    /** Specifies a function that is invoked when the collector is paused. */
    readonly onPaused?: (snapshot: ISnapshot<ICollectorSnapshot>) => void;

    /** Specifies a function that is invoked when the edit button of a block in preview mode is clicked. */
    readonly onEditRequest?: (nodeId: string) => void;

    /** Specifies attachment related settings. */
    readonly onAttachment?: {
        /** Specifies the function to retrieve an attachment. */
        get: (file: string) => Promise<Blob>;

        /** Specifies the function that is invoked when a file is attached. */
        put: (file: File, onProgress: (percent?: number) => void) => Promise<string>;

        /** Specifies the function that is invoked when an attachment is deleted. */
        delete: (file: string) => Promise<void>;
    };
}> {
    private readonly blocks = new Blocks(this);
    private readonly onChange = this.props.onChange && new Debounce(() => this.props.onChange!(this));
    private readonly pauseDialog: React.RefObject<HTMLDivElement> = React.createRef();
    private onPauseDialog?: (emailAddress: string) => void;
    private reloading = false;
    readonly element = React.createRef<HTMLElement>();
    readonly state: ICollectorState = {
        style: this.props.style || {},
        overrides: this.props.overrides || {},
        view: this.props.view || "normal",
        mode: this.props.mode || "paginated",
        changes: 0,
        focus: {}
    };

    /** Retrieves the definition. */
    get definition(): IDefinition | undefined {
        return this.blocks.definition;
    }

    /** Retrieves the name of the definition. */
    get name(): string {
        return this.blocks.name;
    }

    /** Retrieves if the collector is empty. */
    get isEmpty(): boolean {
        return this.blocks.isEmpty;
    }

    /** Retrieves if the collector is running. */
    get isRunning(): boolean {
        return this.blocks.isRunning;
    }

    /** Retrieves if the collector is stopped. */
    get isStopped(): boolean {
        return this.blocks.isStopped;
    }

    /** Retrieves if the collector has finished (without pausing or stopping it). */
    get isFinished(): boolean {
        return this.blocks.isFinished;
    }

    /** Retrieves if the collector is reloading. */
    get isReloading(): boolean {
        return this.reloading;
    }

    /** Retrieves the current style. */
    get style(): ICollectorStyle {
        return this.state.style;
    }

    /** Sets the current style. */
    set style(style: ICollectorStyle) {
        this.setState({
            style
        });
    }

    /** Retrieves the current overrides. */
    get overrides(): ICollectorOverrides {
        return this.state.overrides;
    }

    /** Sets the current overrides. */
    set overrides(overrides: ICollectorOverrides) {
        this.setState({
            overrides
        });
    }

    /** Retrieves the current view mode. */
    get view(): "normal" | "preview" | "test" {
        return this.state.view;
    }

    /** Sets the current view mode. */
    set view(view: "normal" | "preview" | "test") {
        this.setState({
            view
        });
    }

    /** Retrieves the current operation mode. */
    get mode(): "paginated" | "continuous" | "progressive" {
        return this.state.mode;
    }

    /** Sets the current operation mode. */
    set mode(mode: "paginated" | "continuous" | "progressive") {
        this.setState({
            mode
        });
    }

    render(): React.ReactNode {
        const onDragAndDrop = (event: React.DragEvent) => {
            event.preventDefault();
        };

        return (
            <section
                className={`tripetto-collector-standard-bootstrap${this.props.usage === "preview" ? " preview" : ""}`}
                ref={this.element}
                style={{
                    backgroundColor: this.style.backgroundColor || undefined,
                    backgroundImage:
                        this.style.backgroundImage && this.style.backgroundImage.url
                            ? "url(" + this.style.backgroundImage.url + ")"
                            : undefined,
                    backgroundSize:
                        this.style.backgroundImage && this.style.backgroundImage.url && this.style.backgroundImage.size !== "repeat"
                            ? this.style.backgroundImage.size
                            : undefined,
                    backgroundRepeat:
                        this.style.backgroundImage && this.style.backgroundImage.url && this.style.backgroundImage.size === "repeat"
                            ? "repeat"
                            : undefined,
                    fontFamily: this.style.textFont || undefined,
                    color: this.style.textColor || undefined
                }}
                onDragEnter={onDragAndDrop}
                onDragOver={onDragAndDrop}
                onDragLeave={onDragAndDrop}
                onDrop={onDragAndDrop}
            >
                <div
                    className={`container${
                        this.props.usage === "frame" ? " frame-usage" : this.props.usage !== "standalone" ? " inline-usage" : ""
                    }`}
                >
                    {this.blocks.render() ||
                        ((this.blocks.status === "empty" || this.blocks.status === "preview") && emptyMessage(this.view)) ||
                        (this.blocks.status === "finished" && finishedMessage(this.style, this.overrides)) ||
                        (this.blocks.status === "paused" && pausedMessage()) ||
                        stoppedMessage()}
                    {(this.props.onPause || this.props.onPaused) &&
                        pauseDialog(this.pauseDialog, this.style, (emailAddress: string) => {
                            if (this.onPauseDialog) {
                                this.onPauseDialog(emailAddress);
                            }
                        })}
                </div>
            </section>
        );
    }

    componentDidMount(): void {
        this.blocks.onChange = () =>
            this.setState((prevState: ICollectorState) => ({
                changes: prevState.changes + 1
            }));

        this.blocks.onFinish = (instance: Instance) => {
            if (this.props.onFinish) {
                this.props.onFinish(instance);
            }
        };

        if (this.props.onReady) {
            this.props.onReady(this);
        }
    }

    componentDidUpdate(): void {
        this.blocks.componentDidUpdate();

        if (this.onChange) {
            this.onChange.invoke();
        }
    }

    componentWillUnmount(): void {
        this.blocks.destroy();
    }

    /** Start the collector. */
    start(): void {
        this.blocks.start();
    }

    /** Resume a collector that was previously paused. */
    resume(snapshot: ISnapshot<ICollectorSnapshot>): boolean {
        return this.blocks.resume(snapshot);
    }

    /** Pauses the collector. */
    pause(): ISnapshot<ICollectorSnapshot> | undefined {
        const snapshot = this.blocks.pause<ICollectorSnapshot>({
            b: this.state.focus
        });

        if (snapshot && this.props.onPaused) {
            this.props.onPaused(snapshot);
        }

        return snapshot;
    }
    /** Requests a pause. */
    requestPause(): void {
        if (!this.props.onPause || !castToBoolean(this.props.onPause(this))) {
            this.pause();
        }
    }

    /** Stop the collector. */
    stop(): void {
        this.blocks.stop();
    }

    /** Restarts the collector. */
    restart(): void {
        this.blocks.restart(false);
    }

    /** Reload with a new definition. */
    reload(definition: IDefinition): void {
        this.reloading = true;

        this.blocks.reload(definition);

        this.reloading = false;
    }

    /** Call this function when the parent element is resized. */
    resize(): void {
        this.forceUpdate();
    }

    /** Shows the pause dialog. */
    showPauseDialog(whenDone: (emailAddress: string, collector: this) => void): void {
        this.onPauseDialog = (emailAddress: string) => {
            this.onPauseDialog = undefined;

            whenDone(emailAddress, this);
        };

        if (this.pauseDialog.current) {
            this.pauseDialog.current.classList.add("pause-dialog-visible");
        }
    }

    /** Request a preview of the specified node. */
    requestPreview(nodeId: string): void {
        const storyline = this.blocks.storyline;

        if (storyline) {
            this.setState({
                activate: storyline.getKeyByNodeId(nodeId)
            });
        }
    }
}
