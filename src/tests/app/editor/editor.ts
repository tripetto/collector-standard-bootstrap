import { Editor, IDefinition } from "tripetto";
import "./editor.scss";

export const create = (element: HTMLElement | null, definition?: IDefinition) =>
    Editor.open(definition, {
        element: element,
        disableSaveButton: true,
        disableRestoreButton: true,
        disableClearButton: false,
        disableCloseButton: true,
        disableOpenCloseAnimation: true,
        zoom: "fit-horizontal"
    });
