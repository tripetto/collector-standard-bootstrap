import { ICollectorStyle } from "./style";
import { ICollectorOverrides } from "./overrides";

export interface ICollectorState {
    style: ICollectorStyle;
    overrides: ICollectorOverrides;
    view: "normal" | "preview" | "test";
    mode: "paginated" | "continuous" | "progressive";
    changes: number;
    focus: {
        [key: string]: boolean | undefined;
    };
    activate?: string;
}
