import * as React from "react";
import { ICollectorStyle } from "interfaces/style";

export interface IBlockHelper {
    /** Contains the style. */
    style: ICollectorStyle;

    /** Parsed markdown name. */
    name: (required?: boolean, labelFor?: string) => React.ReactNode;

    /** Parsed markdown description. */
    description: React.ReactNode;

    /** Parsed image. */
    imageFromURL: (imageURL?: string, imageWidth?: string) => React.ReactNode;

    /** Parsed markdown explanation. */
    explanation: (label?: string) => React.ReactNode;

    /** Parsed markdown placeholder. */
    placeholder: string;

    /** Parsed markdown placeholder or name (single line). */
    placeholderOrName: (required?: boolean) => React.ReactNode;

    /** Tab-index for the block. */
    tabIndex: number;

    /** Specifies if the block validation failed. */
    isFailed: boolean;

    /** Updates a block. */
    update: () => void;

    /** The block gets focus. */
    onFocus: () => void;

    /** The block loses focus. */
    onBlur: () => void;

    /** Specifies the function that is invoked when a file is attached. */
    onAttachment?: {
        get: (file: string) => Promise<Blob>;
        put: (file: File, onProgress: (percent?: number) => void) => Promise<string>;
        delete: (file: string) => Promise<void>;
    };
}
