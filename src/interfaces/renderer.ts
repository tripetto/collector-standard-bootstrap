import { NodeBlock } from "tripetto-collector";
import { IBlockHelper } from "./block";

export interface IBlockRenderer extends NodeBlock {
    render: (block: IBlockHelper) => React.ReactNode;
}
