import * as React from "react";
import { Storyline, pgettext } from "tripetto-collector";
import { IBlockRenderer } from "../interfaces/renderer";
import { ICollectorStyle } from "../interfaces/style";
import { ICON_PAUSE } from "./icons/pause";

export const buttons = (
    storyline: Storyline<IBlockRenderer>,
    style: ICollectorStyle,
    view: "normal" | "preview" | "test",
    fnPause?: () => void
) => (
    <div className="col-md-auto pb-3 pb-md-0">
        <div role="group" aria-label="Navigation">
            {storyline.mode === "progressive" ? (
                <button
                    type="button"
                    className={`btn btn-${(style.buttons && style.buttons.completeStyle) || "success"}${fnPause ? " mr-2" : ""}`}
                    disabled={!storyline.isFinishable}
                    onClick={() => storyline.finish()}
                >
                    {(style.buttons && style.buttons.completeLabel) || pgettext("collector-standard-bootstrap", "Complete")}
                </button>
            ) : (
                <>
                    {!storyline.isAtStart && (
                        <button
                            type="button"
                            className={`btn btn-${(style.buttons && style.buttons.backStyle) || "light"} mr-2`}
                            disabled={storyline.isAtStart}
                            onClick={() => storyline.stepBackward()}
                        >
                            {(style.buttons && style.buttons.backLabel) || pgettext("collector-standard-bootstrap", "Back")}
                        </button>
                    )}
                    <button
                        type="button"
                        className={`btn btn-${(style.buttons &&
                            (storyline.isAtFinish ? style.buttons.completeStyle : style.buttons.nextStyle)) ||
                            (storyline.isAtFinish ? "success" : "primary")}${fnPause ? " mr-2" : ""}`}
                        disabled={storyline.isFailed || (storyline.isAtFinish && !storyline.isFinishable)}
                        onClick={() => storyline.stepForward()}
                    >
                        {storyline.isAtFinish
                            ? (style.buttons && style.buttons.completeLabel) || pgettext("collector-standard-bootstrap", "Complete")
                            : (style.buttons && style.buttons.nextLabel) || pgettext("collector-standard-bootstrap", "Next")}
                    </button>
                </>
            )}
            {fnPause && (
                <button
                    className={`btn btn-${(style.buttons && style.buttons.pauseStyle) || "light"}`}
                    disabled={view !== "normal" || storyline.isEvaluating}
                    onClick={fnPause}
                >
                    {ICON_PAUSE}
                </button>
            )}
        </div>
    </div>
);
