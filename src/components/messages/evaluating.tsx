import * as React from "react";
import { pgettext } from "tripetto-collector";

export const evaluatingMessage = () => (
    <div className="text-center mt-5">
        <p className="text-secondary">{pgettext("collector-standard-bootstrap", "⏳ Please wait...")}</p>
    </div>
);
