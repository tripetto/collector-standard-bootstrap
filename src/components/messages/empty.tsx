import * as React from "react";
import { pgettext } from "tripetto-collector";

export const emptyMessage = (view: "normal" | "preview" | "test") => (
    <div className="row justify-content-center align-items-center message">
        <div className="col-md-8 col-lg-10">
            <div className="text-center">
                {view === "normal" ? (
                    <>
                        <h2>{pgettext("collector-rolling", "👋 Hi there")}</h2>
                        <h3>{pgettext("collector-rolling", "This form happens to be empty")}</h3>
                        <p className="text-faded">
                            {pgettext("collector-rolling", "Nothing to do here for now. Thanks for visiting though.")}
                        </p>
                    </>
                ) : (
                    <>
                        <h2>{pgettext("collector-rolling", "👋 Nothing to show")}</h2>
                        <h3>{pgettext("collector-rolling", "The form is empty")}</h3>
                        <p className="text-faded">
                            {pgettext("collector-rolling", "Add blocks to the form first to get the magic going.")}
                        </p>
                    </>
                )}
            </div>
        </div>
    </div>
);
