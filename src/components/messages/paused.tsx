import * as React from "react";
import { pgettext } from "tripetto-collector";

export const pausedMessage = () => (
    <div className="row justify-content-center align-items-center message">
        <div className="col-md-8 col-lg-6">
            <div className="text-center">
                <h2>{pgettext("collector-standard-bootstrap", "🚧 Paused")}</h2>
                <h3>{pgettext("collector-standard-bootstrap", "The form was paused")}</h3>
            </div>
        </div>
    </div>
);
