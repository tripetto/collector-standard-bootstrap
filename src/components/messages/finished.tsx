import * as React from "react";
import { pgettext } from "tripetto-collector";
import { ICollectorStyle } from "../../interfaces/style";
import { ICollectorOverrides } from "../../interfaces/overrides";

export const finishedMessage = (style: ICollectorStyle, overrides: ICollectorOverrides) => (
    <div className="row justify-content-center align-items-center message">
        <div className="col-md-8 col-lg-10">
            <div className="text-center">
                <h2>{overrides.confirmationTitle || pgettext("collector-rolling", "✅ Form completed")}</h2>
                {overrides.confirmationSubtitle !== "" && (
                    <h3>{overrides.confirmationSubtitle || pgettext("collector-rolling", "Thank you")}</h3>
                )}
                {overrides.confirmationText && <p className="text-faded my-0">{overrides.confirmationText}</p>}
                {overrides.confirmationImage && <img src={overrides.confirmationImage} className="img-fluid border rounded" />}
                {!overrides.removeBranding && (
                    <>
                        <hr
                            className="my-5"
                            style={{
                                borderColor: style.textColor || undefined
                            }}
                        ></hr>
                        <h4 className="pb-1">{pgettext("collector-rolling", "Want to make a form like this for free?")}</h4>
                        <p className="text-faded my-0">
                            {pgettext(
                                "collector-rolling",
                                "Tripetto is for making elegantly personal form and survey experiences with response boosting conversational powers."
                            )}
                        </p>
                        <p className="text-faded text-bold mt-0 mb-4">
                            {pgettext("collector-rolling", "And it's completely free for everyone!")}
                        </p>
                    </>
                )}
                {(!overrides.removeBranding || (overrides.confirmationButton && overrides.confirmationButton !== "off")) && (
                    <div>
                        <button
                            className={`btn btn-${(style.buttons && style.buttons.nextStyle) || "primary"} mt-4`}
                            role="button"
                            onClick={() =>
                                window.open(
                                    (overrides.confirmationButton &&
                                        overrides.confirmationButton !== "off" &&
                                        overrides.confirmationButton.url) ||
                                        "https://tripetto.app"
                                )
                            }
                        >
                            {overrides.confirmationButton && overrides.confirmationButton !== "off"
                                ? overrides.confirmationButton.label || pgettext("collector-rolling", "Done")
                                : pgettext("collector-rolling", "Create a form")}
                        </button>
                    </div>
                )}
                {!overrides.removeBranding && (
                    <a
                        href="https://tripetto.com"
                        className="link"
                        style={{
                            color: style.textColor
                        }}
                        target="_blank"
                    >
                        {pgettext("collector-rolling", "Learn more")}
                    </a>
                )}
            </div>
        </div>
    </div>
);
