import * as React from "react";
import { Storyline } from "tripetto-collector";
import { IBlockRenderer } from "../interfaces/renderer";
import { ICollectorStyle } from "../interfaces/style";

export const progressbar = (
    mode: "paginated" | "continuous" | "progressive",
    storyline: Storyline<IBlockRenderer>,
    style: ICollectorStyle
) => {
    const percentage = mode !== "paginated" && storyline.isFinishable ? 100 : storyline.percentage;

    return (
        <div className="col pb-3 pb-md-0">
            <div className="progress">
                <div
                    className={`progress-bar bg-${(style.navigation && style.navigation.progressbarStyle) || "primary"}`}
                    role="progressbar"
                    aria-valuenow={percentage}
                    aria-valuemin={0}
                    aria-valuemax={100}
                    style={{ width: `${percentage}%`, minWidth: "2em" }}
                >
                    {percentage}%
                </div>
            </div>
        </div>
    );
};
