import * as React from "react";
import { IPage, Storyline } from "tripetto-collector";
import { IBlockRenderer } from "../interfaces/renderer";
import { ICollectorStyle } from "../interfaces/style";

export const pagination = (storyline: Storyline<IBlockRenderer>, style: ICollectorStyle) =>
    storyline.pages.length > 1 && (
        <div className="col-md-auto pb-3 pb-md-0">
            <div className="btn-group mb-0" role="group">
                {storyline.pages.map((page: IPage) => (
                    <button
                        type="button"
                        className={`btn btn-normal btn-${(style.navigation && style.navigation.paginationStyle) || "outline-primary"}${
                            page.active ? " active" : ""
                        }`}
                        onClick={() => page.activate()}
                    >
                        {page.number}
                    </button>
                ))}
            </div>
        </div>
    );
