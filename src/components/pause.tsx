import { Collector } from "../collector";
import { ISnapshot, pgettext } from "tripetto-collector";
import { ICollectorStyle } from "../interfaces/style";
import { ICollectorSnapshot } from "../interfaces/snapshot";
import * as React from "react";

export const pauseDialog = (
    ref: React.RefObject<HTMLDivElement>,
    style: ICollectorStyle,
    whenPauseDialogDone: (emailAddress: string) => void
) => {
    const pauseRecipient = React.createRef<HTMLInputElement>();
    const pauseSubmit = React.createRef<HTMLButtonElement>();
    const fnVerify = () => {
        if (pauseRecipient.current) {
            const value = pauseRecipient.current.value || "";
            const isValid =
                value &&
                /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
                    value
                );

            if (pauseSubmit.current) {
                pauseSubmit.current.classList.toggle("disabled", !isValid);
            }

            return isValid;
        }

        return false;
    };
    const fnClose = () => {
        if (ref.current) {
            ref.current.classList.remove("pause-dialog-visible");
        }
    };
    const fnSubmit = () => {
        if (fnVerify()) {
            fnClose();

            if (whenPauseDialogDone && pauseRecipient.current) {
                whenPauseDialogDone(pauseRecipient.current.value);
            }
        }
    };

    return (
        <section className="pause-dialog" ref={ref}>
            <div className="pause-dialog-backdrop" />
            <div className="pause-dialog-window" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div
                        className="modal-content"
                        style={{
                            backgroundColor:
                                style.backgroundColor === ""
                                    ? undefined
                                    : style.backgroundColor === "transparent"
                                    ? "#ffffff"
                                    : style.backgroundColor,
                            fontFamily: style.textFont || undefined,
                            color: style.textColor || undefined
                        }}
                    >
                        <div className="modal-body my-3 text-center">
                            <div className="container">
                                <div className="row">
                                    <div className="col">
                                        <h2>{pgettext("collector-standard-bootstrap", "🚦 Standby...")}</h2>
                                        <h3>{pgettext("collector-standard-bootstrap", "You're pausing the form")}</h3>
                                        <p className="text-faded">
                                            {pgettext(
                                                "collector-standard-bootstrap",
                                                "Receive a link by email to resume later on any device, right where you left off. "
                                            )}
                                            <strong>
                                                {pgettext("collector-standard-bootstrap", "Your email address will not be stored.")}
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div className="row justify-content-sm-center">
                                    <div className="col-sm-8">
                                        <input
                                            type="email"
                                            ref={pauseRecipient}
                                            className={`form-control form-control-${(style.form && style.form.inputStyle) || "default"}`}
                                            placeholder={pgettext("collector-standard-bootstrap", "Your email address...")}
                                            onChange={() => fnVerify()}
                                            onKeyPress={(event: React.KeyboardEvent) => {
                                                if (event.key === "Enter") {
                                                    fnSubmit();
                                                }
                                            }}
                                        />
                                        <button
                                            className={`btn btn-${(style.buttons && style.buttons.nextStyle) ||
                                                "primary"} btn-block disabled`}
                                            ref={pauseSubmit}
                                            onClick={() => fnSubmit()}
                                        >
                                            {pgettext("collector-standard-bootstrap", "Receive resume link")}
                                        </button>
                                        <span
                                            className="link"
                                            style={{
                                                color: style.textColor
                                            }}
                                            onClick={() => fnClose()}
                                        >
                                            {pgettext("collector-standard-bootstrap", "Resume now anyway")}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export const usePauseDialog = (saveSnapshot: (emailAddress: string, snapshot: ISnapshot<ICollectorSnapshot>) => void) => (
    collector: Collector
) => {
    collector.showPauseDialog((emailAddress: string) => {
        const snapshot = collector.pause();

        if (snapshot) {
            saveSnapshot(emailAddress, snapshot);
        }
    });

    return true;
};
