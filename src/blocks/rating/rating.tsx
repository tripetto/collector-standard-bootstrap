import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Rating } from "tripetto-block-rating/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { ICON_UNCHECKED } from "./icon-unchecked";
import { ICON_CHECKED } from "./icon-checked";
import "./rating.scss";

@tripetto({
    type: "node",
    identifier: "tripetto-block-rating",
    alias: "rating",
    autoRender: true
})
export class RatingBlock extends Rating implements IBlockRenderer {
    private buttons(buttons: number, block: IBlockHelper): React.ReactNode[] {
        const r: React.ReactNode[] = [];

        for (let button = 1; button <= buttons; button++) {
            r.push(
                <button
                    key={this.key(`${button}`)}
                    tabIndex={block.tabIndex}
                    className={`rating-${
                        this.ratingSlot.value >= button
                            ? `${(block.style.form && block.style.form.positiveStyle.replace("outline-", "")) || "default"} selected`
                            : `${(block.style.form && block.style.form.neutralStyle.replace("outline-", "")) || "default"}`
                    }`}
                    onClick={() => {
                        const nRating = this.ratingSlot.value === button ? button - 1 : button;

                        this.ratingSlot.pristine = nRating ? nRating : undefined;
                    }}
                >
                    {this.ratingSlot.value >= button ? ICON_CHECKED : ICON_UNCHECKED}
                    <span>{button}</span>
                </button>
            );
        }

        return r;
    }

    render(block: IBlockHelper): React.ReactNode {
        const buttons = 5;

        return (
            <div className="form-group tripetto-collector-rating">
                {this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                {block.name(this.required)}
                {block.description}
                {!this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                <div>{this.buttons(buttons, block)}</div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
