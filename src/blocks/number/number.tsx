import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Number } from "tripetto-block-number/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-number"
})
export class NumberBlock extends Number implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <input
                    type="text"
                    id={this.key()}
                    key={this.key()}
                    required={this.required}
                    inputMode={this.stepSize === "1" ? "numeric" : "decimal"}
                    defaultValue={this.value}
                    tabIndex={block.tabIndex}
                    placeholder={block.placeholder || "#"}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.value = e.target.value;
                    }}
                    onFocus={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onFocus();

                        this.focus();

                        // Switch to number type when focus is gained.
                        e.target.value = this.value;
                        e.target.type = "number";
                        e.target.step = this.stepSize;
                    }}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onBlur();

                        this.blur();

                        // Switch to text type to allow number prefix and suffix.
                        e.target.type = "text";
                        e.target.value = this.value;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
