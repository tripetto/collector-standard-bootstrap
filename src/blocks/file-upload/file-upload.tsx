import * as React from "react";
import * as path from "path";
import { Callback, IValidatorHandler, NodeBlock, ValidatorResult, assert, pgettext, tripetto, validator } from "tripetto-collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { ICON_FILE } from "./icon-file";
import { ICON_BROWSE } from "./icon-browse";
import "tripetto-block-file-upload/collector";
import "./file-upload.scss";

type TErrors = "EXTENSION" | "SIZE" | "AMOUNT" | "UPLOAD";

@tripetto({
    type: "node",
    identifier: "tripetto-block-file-upload",
    alias: "file-upload"
})
export class FileUploadBlock
    extends NodeBlock<{
        limit?: number;
        extensions?: string;
    }>
    implements IBlockRenderer {
    private error?: TErrors;
    private errorMessage?: string;
    private isDownloading = false;
    private isDragging = false;
    private percentageUploaded = 0;
    private thumbnail: {
        dataUrl?: string;
        isImage?: boolean;
    } = {};
    private validatorCallback?: Callback<boolean>;

    readonly fileSlot = assert(this.valueOf<string>("file"));
    readonly statusSlot = assert(this.valueOf<boolean>("status"));

    private get isUploading(): boolean {
        return this.statusSlot.value;
    }

    private get limit(): number {
        return this.props.limit || 10;
    }

    render(block: IBlockHelper): React.ReactNode {
        if (this.fileSlot.hasValue && !this.thumbnail.dataUrl) {
            const isImage = this.hasImageExtension(this.fileSlot.string);

            if (isImage === true) {
                if (block.onAttachment && this.fileSlot.reference) {
                    if (this.isDownloading === false) {
                        this.isDownloading = true;

                        block.onAttachment
                            .get(this.fileSlot.reference)
                            .then((blob: Blob) => {
                                this.readAsDataUrl(blob, (dataUrl: string) => {
                                    this.thumbnail.dataUrl = dataUrl;
                                    this.thumbnail.isImage = isImage;

                                    block.update();
                                });

                                this.isDownloading = false;
                            })
                            .catch(() => {
                                this.fileSlot.set(undefined);
                                block.update();

                                this.isDownloading = false;
                            });
                    }
                } else {
                    this.thumbnail.dataUrl = this.fileSlot.reference;
                    this.thumbnail.isImage = isImage;
                }
            }
        }

        const onFileChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
            if (event.target && event.target.files) {
                this.onFileUpload(event.target.files, block);
            }
        };

        const onFileDelete = () => {
            this.thumbnail = {};
            this.percentageUploaded = 0;
            this.setError(block, undefined);

            if (block.onAttachment && this.fileSlot.reference) {
                block.onAttachment.delete(this.fileSlot.reference).then(() => this.fileSlot.set(undefined));
            } else {
                this.fileSlot.set(undefined);
            }
        };

        const onDrop = (event: React.DragEvent) => {
            event.preventDefault();
            event.stopPropagation();

            this.isDragging = false;

            const files = event.dataTransfer.files;
            if (files && files.length > 0) {
                this.onFileUpload(files, block);
                event.dataTransfer.clearData();
            }
        };

        const onDrag = (event: React.DragEvent) => {
            event.preventDefault();
            event.stopPropagation();
        };

        const onDragEnter = (event: React.DragEvent) => {
            event.preventDefault();
            event.stopPropagation();

            this.isDragging = true;
            block.update();
        };

        const onDragLeave = (event: React.DragEvent) => {
            event.preventDefault();
            event.stopPropagation();

            this.isDragging = false;
            block.update();
        };

        return (
            <div className="form-group tripetto-collector-file-upload" tabIndex={block.tabIndex}>
                {block.name(this.fileSlot.slot.required)}
                {block.description}
                <div
                    className={`file-upload-area alert alert-${(block.style.form && block.style.form.inputStyle) || "primary"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                >
                    {this.fileSlot.hasValue === false && this.isUploading === false && !this.error && (
                        <div className="file-upload-input">
                            {ICON_BROWSE}
                            <input type="file" id={this.key("file")} onChange={onFileChanged} />
                            <label
                                htmlFor={this.key("file")}
                                onDragEnter={onDragEnter}
                                onDragOver={onDrag}
                                onDragLeave={onDragLeave}
                                onDrop={onDrop}
                                className={this.isDragging ? "file-dragging" : ""}
                            />
                            <span>
                                {pgettext(
                                    "collector-standard-bootstrap",
                                    this.isDragging ? "Drop your file now" : "Choose a file or drag a file here"
                                )}
                            </span>
                            <small>{pgettext("collector-standard-bootstrap", "Size limit: %1", `${this.limit}Mb`)}</small>
                            {this.props.extensions && (
                                <small>
                                    {pgettext(
                                        "collector-standard-bootstrap",
                                        "Allowed extensions: %1",
                                        `${this.formatExtensions(this.props.extensions)}`
                                    )}
                                </small>
                            )}
                        </div>
                    )}
                    {this.error && (
                        <div className="file-upload-error">
                            <strong>{pgettext("collector-standard-bootstrap", "This file can't be used")}</strong>
                            <div>{this.getError(this.error)}</div>
                            <button
                                className={`btn btn-${(block.style.form && block.style.form.inputStyle) || "primary"} btn-normal`}
                                onClick={() => this.setError(block, undefined)}
                            >
                                {pgettext("collector-standard-bootstrap", "Try again")}
                            </button>
                        </div>
                    )}
                    {this.isUploading === true && (
                        <div className="file-upload-progress">
                            <div className="progress">
                                <div
                                    className={`progress-bar progress-bar-striped progress-bar-animated bg-${(block.style.form &&
                                        block.style.form.inputStyle) ||
                                        "primary"}`}
                                    role="progressbar"
                                    aria-valuenow={this.percentageUploaded}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                    style={{ width: `${this.percentageUploaded}%`, minWidth: "2em" }}
                                />
                            </div>
                            <div className="progress-text">
                                {pgettext("collector-standard-bootstrap", "Progress: %1", `${this.percentageUploaded}%`)}
                            </div>
                        </div>
                    )}
                    {this.fileSlot.hasValue && (
                        <div className="file-upload-preview">
                            {(this.thumbnail.isImage && this.thumbnail.isImage === true && (
                                <div className="preview-image">
                                    <img src={this.thumbnail.dataUrl} className="img-thumbnail" />
                                </div>
                            )) || <div className="preview-file">{ICON_FILE}</div>}
                            <div className="preview-name text-truncate">{this.fileSlot.string}</div>
                            <button
                                className={`btn btn-${(block.style.form && block.style.form.inputStyle) || "primary"} btn-normal`}
                                onClick={onFileDelete}
                            >
                                {pgettext("collector-standard-bootstrap", "Delete")}
                            </button>
                        </div>
                    )}
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }

    hasValidFileSize(file: File): boolean {
        const maximumFileBytes = this.limit * 1024 * 1024;
        const hasValidSize = file.size <= maximumFileBytes;
        return hasValidSize;
    }

    hasValidFileExtension(file: File): boolean {
        const fileExtension = path.extname(file.name).toLowerCase();

        let hasValidFileExtension = true;
        if (this.props.extensions) {
            hasValidFileExtension =
                this.formatExtensions(this.props.extensions)
                    .split(",")
                    .indexOf(fileExtension) > -1;
        }

        return hasValidFileExtension;
    }

    formatExtensions(extensions?: string): string {
        return extensions ? extensions.toLowerCase().replace(" ", "") : "";
    }

    onFileUpload(files: FileList, block: IBlockHelper): void {
        if (files.length > 1) {
            this.setError(block, "AMOUNT");
            return;
        }

        const file = files[0];
        if (this.hasValidFileExtension(file) === false) {
            this.setError(block, "EXTENSION");
            return;
        } else if (this.hasValidFileSize(file) === false) {
            this.setError(block, "SIZE");
            return;
        }

        const onProgress = (percent?: number) => {
            if (percent) {
                this.percentageUploaded = Math.floor(percent);
                block.update();
            }
        };

        this.statusSlot.value = true;
        this.thumbnail.isImage = this.hasImageExtension(file.name);

        if (block.onAttachment) {
            block.onAttachment
                .put(file, onProgress)
                .then((id: string) => {
                    this.fileSlot.set(file.name, id);

                    if (this.validatorCallback) {
                        this.validatorCallback.return(true);
                    }

                    this.statusSlot.value = false;
                })
                .catch((error: string) => {
                    this.setError(block, "UPLOAD", error);

                    if (this.validatorCallback) {
                        this.validatorCallback.return(false);
                    }

                    this.statusSlot.value = false;
                });

            if (this.thumbnail.isImage && this.thumbnail.isImage === true) {
                this.readAsDataUrl(file, (data: string) => {
                    this.thumbnail.dataUrl = data;
                });
            }
        } else {
            this.readAsDataUrl(
                file,
                (data: string) => {
                    if (this.validatorCallback) {
                        this.validatorCallback.return(true);
                    }

                    this.statusSlot.value = false;
                    this.fileSlot.set(file.name, data);
                    this.thumbnail.dataUrl = data;
                },
                onProgress
            );
        }
    }

    readAsDataUrl(blob: Blob, done: (data: string) => void, progress?: (percentage: number) => void): void {
        const reader = new FileReader();
        reader.onprogress = (event: ProgressEvent) => {
            if (progress) {
                progress((event.loaded / event.total) * 100);
            }
        };
        reader.onload = () => {
            done(reader.result as string);
        };
        reader.readAsDataURL(blob);
    }

    hasImageExtension(filename: string): boolean {
        const extension = path.extname(filename);
        return [".jpg", ".jpeg", ".png", ".gif"].indexOf(extension.toLowerCase()) > -1;
    }

    setError(block: IBlockHelper, error: TErrors | undefined, errorMessage?: string): void {
        this.error = error;
        this.errorMessage = errorMessage;

        block.update();
    }

    getError(error: TErrors): string {
        switch (error) {
            case "SIZE":
                return pgettext("collector-standard-bootstrap", "File size is too large.");
            case "EXTENSION":
                return pgettext("collector-standard-bootstrap", "Extension is not allowed.");
            case "AMOUNT":
                return pgettext("collector-standard-bootstrap", "Too many files selected.");
            case "UPLOAD":
                return pgettext(
                    "collector-rolling",
                    `Something went wrong while uploading${(this.errorMessage && ` (server said: ${this.errorMessage})`) || ""}.`
                );
            default:
                return pgettext("collector-standard-bootstrap", "Unknown error.");
        }
    }

    @validator
    validate(handler: IValidatorHandler): ValidatorResult {
        if (this.isUploading) {
            return (this.validatorCallback = handler.callback);
        }

        return !this.fileSlot.slot.required || this.fileSlot.hasValue;
    }
}
