import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Checkbox } from "tripetto-block-checkbox/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-checkbox",
    alias: "checkbox"
})
export class CheckboxBlock extends Checkbox implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.placeholder && block.name()}
                {block.description}
                <div className="custom-control custom-checkbox">
                    <input
                        key={this.key()}
                        id={this.key()}
                        type="checkbox"
                        defaultChecked={this.checkboxSlot.value}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            this.checkboxSlot.value = e.target.checked;
                        }}
                        className={`custom-control-input checkbox-control-${(block.style.form && block.style.form.inputStyle) ||
                            "default"}`}
                        aria-describedby={this.node.explanation && this.key("explanation")}
                        tabIndex={block.tabIndex}
                    />
                    <label htmlFor={this.key()} className="custom-control-label">
                        {block.placeholderOrName(this.required)}
                    </label>
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
