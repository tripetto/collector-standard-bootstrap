import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Email } from "tripetto-block-email/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-email"
})
export class EmailBlock extends Email implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <input
                    type="email"
                    id={this.key()}
                    key={this.key()}
                    required={this.required}
                    inputMode="email"
                    defaultValue={this.emailSlot.value}
                    tabIndex={block.tabIndex}
                    placeholder={block.placeholder || "@"}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.emailSlot.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onBlur();

                        e.target.value = this.emailSlot.string;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
