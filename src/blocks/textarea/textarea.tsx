import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Textarea } from "tripetto-block-textarea/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-textarea"
})
export class TextareaBlock extends Textarea implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <textarea
                    id={this.key()}
                    key={this.key()}
                    rows={3}
                    required={this.required}
                    defaultValue={this.textareaSlot.value}
                    placeholder={block.placeholder}
                    tabIndex={block.tabIndex}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
                        this.textareaSlot.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
                        block.onBlur();

                        e.target.value = this.textareaSlot.string;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
