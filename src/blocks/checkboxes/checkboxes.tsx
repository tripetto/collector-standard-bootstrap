import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Checkboxes, ICheckbox } from "tripetto-block-checkboxes/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-checkboxes"
})
export class CheckboxesBlock extends Checkboxes implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name()}
                {block.description}
                <div>
                    {this.props.checkboxes.map((checkbox: ICheckbox) => {
                        const checkboxSlot = this.checkboxSlot(checkbox);

                        return (
                            <div key={checkboxSlot.key} className="custom-control custom-checkbox">
                                <input
                                    key={checkboxSlot.key}
                                    id={checkboxSlot.key}
                                    type="checkbox"
                                    defaultChecked={this.isChecked(checkbox)}
                                    onChange={() => {
                                        this.toggle(checkbox);
                                    }}
                                    className={`custom-control-input checkbox-control-${(block.style.form && block.style.form.inputStyle) ||
                                        "default"}`}
                                    aria-describedby={this.node.explanation && this.key("explanation")}
                                    tabIndex={block.tabIndex}
                                />
                                <label htmlFor={checkboxSlot.key} className="custom-control-label">
                                    {checkbox.name || "..."}
                                </label>
                            </div>
                        );
                    })}
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
