import * as React from "react";
import { tripetto } from "tripetto-collector";
import { IRadiobutton, Radiobuttons } from "tripetto-block-radiobuttons/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-radiobuttons"
})
export class RadiobuttonsBlock extends Radiobuttons implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required)}
                {block.description}
                <div>
                    {this.props.buttons.map(
                        (radiobutton: IRadiobutton) =>
                            radiobutton.name && (
                                <div key={this.key(radiobutton.id)} className="custom-control custom-radio">
                                    <input
                                        type="radio"
                                        key={this.key(radiobutton.id)}
                                        id={this.key(radiobutton.id)}
                                        tabIndex={block.tabIndex}
                                        name={this.key()}
                                        defaultChecked={this.isSelected(radiobutton)}
                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                            this.select(radiobutton);
                                        }}
                                        className={`custom-control-input checkbox-control-${(block.style.form &&
                                            block.style.form.inputStyle) ||
                                            "default"}`}
                                        aria-describedby={this.node.explanation && this.key("explanation")}
                                    />
                                    <label htmlFor={this.key(radiobutton.id)} className="custom-control-label">
                                        {radiobutton.name}
                                    </label>
                                </div>
                            )
                    )}
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
