import * as React from "react";
import { assert, tripetto } from "tripetto-collector";
import { IChoice, MultipleChoice } from "tripetto-block-multiple-choice/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { markdownToJSX } from "../../helpers/markdown";
import { ICON_EXTERNAL } from "./icon-external";
import "./multiple-choice.scss";

@tripetto({
    type: "node",
    identifier: "tripetto-block-multiple-choice",
    alias: "multiple-choice",
    autoRender: true
})
export class MultipleChoiceBlock extends MultipleChoice implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group tripetto-collector-multiple-choice">
                {this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                {block.name(this.props.required)}
                {this.props.caption && <h3>{markdownToJSX(this.props.caption || "...", this.context)}</h3>}
                {block.description}
                {!this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                <div>
                    {this.props.choices.map((choice: IChoice) => {
                        const input = assert(this.props.multiple ? this.valueOf<boolean>(choice.id) : this.valueOf<string>("choice"));

                        const selected = this.props.multiple
                            ? input.value
                            : input.reference
                            ? input.reference === choice.id
                            : input.value && input.value === (choice.value || choice.name);

                        if (this.props.multiple) {
                            /**
                             * In this case the data in the slot is confirmed
                             * as soon as the choice was shown to the user.
                             */
                            input.confirm();
                        } else if (selected) {
                            input.set(choice.value || choice.name, choice.id);
                        }

                        return (
                            <div
                                key={this.key(choice.id)}
                                tabIndex={block.tabIndex}
                                className={this.props.alignment ? "d-inline-block" : ""}
                            >
                                <button
                                    className={`btn btn-${
                                        selected
                                            ? `${(block.style.form && block.style.form.selectedStyle) || "success"}`
                                            : `${(block.style.form && block.style.form.unselectedStyle) || "light"}`
                                    } text-left choice mb-2${this.props.alignment ? " mr-2 align-top" : ""}`}
                                    onClick={() => {
                                        if (this.props.multiple) {
                                            input.value = !input.value;
                                        } else {
                                            if (input.reference === choice.id) {
                                                input.set(undefined);
                                            } else {
                                                input.set(choice.value || choice.name, choice.id);
                                            }
                                        }

                                        if (choice.url) {
                                            window.open(choice.url);
                                        }
                                    }}
                                >
                                    <span>
                                        {choice.name || "..."}
                                        {choice.description && <small className="sub">{choice.description}</small>}
                                    </span>
                                    {choice.url && <span className="icon pl-2">{ICON_EXTERNAL}</span>}
                                </button>
                            </div>
                        );
                    })}
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
