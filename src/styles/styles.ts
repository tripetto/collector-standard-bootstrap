import { pgettext } from "tripetto";
import { BOOTSTRAP_STYLES_BASIC, BOOTSTRAP_STYLES_EXTENDED } from "./bootstrap";

export const STYLES = [
    {
        title: pgettext("collector-standard-bootstrap", "Basics"),
        description: pgettext("collector-standard-bootstrap", "Specifies the basic appearance."),
        styles: [
            {
                name: "backgroundColor",
                type: "color",
                title: pgettext("collector-standard-bootstrap", "Background color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the background (transparent, or a color).")
            },
            {
                name: "textFont",
                type: "dropdown-custom",
                title: pgettext("collector-standard-bootstrap", "Text font"),
                description: pgettext("collector-standard-bootstrap", "Specifies the font family."),
                options: [
                    {
                        label: pgettext("collector-standard-bootstrap", "Default"),
                        value: ""
                    },
                    {
                        label: "Arial",
                        value: "Arial, Helvetica, Sans-Serif"
                    },
                    {
                        label: "Arial Black",
                        value: "Arial Black, Gadget, Sans-Serif"
                    },
                    {
                        label: "Comic Sans MS",
                        value: "Comic Sans MS, Textile, Cursive, Sans-Serif"
                    },
                    {
                        label: "Courier New",
                        value: "Courier New, Courier, Monospace"
                    },
                    {
                        label: "Georgia",
                        value: "Georgia, Times New Roman, Times, Serif"
                    },
                    {
                        label: "Impact",
                        value: "Impact, Charcoal, Sans-Serif"
                    },
                    {
                        label: "Lucida Console",
                        value: "Lucida Console, Monaco, Monospace"
                    },
                    {
                        label: "Lucida Sans Unicode",
                        value: "Lucida Sans Unicode, Lucida Grande, Sans-Serif"
                    },
                    {
                        label: "Palatino Linotype",
                        value: "Palatino Linotype, Book Antiqua, Palatino, Serif"
                    },
                    {
                        label: "Tahoma",
                        value: "Tahoma, Geneva, Sans-Serif"
                    },
                    {
                        label: "Times New Roman",
                        value: "Times New Roman, Times, Serif"
                    },
                    {
                        label: "Trebuchet MS",
                        value: "Trebuchet MS, Helvetica, Sans-Serif"
                    },
                    {
                        label: "Verdana",
                        value: "Verdana, Geneva, Sans-Serif"
                    }
                ]
            },
            {
                name: "textColor",
                type: "color",
                title: pgettext("collector-standard-bootstrap", "Text color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the text color.")
            },
            {
                name: "mode",
                type: "radiobuttons",
                title: pgettext("collector-standard-bootstrap", "Mode"),
                buttons: [
                    {
                        label: pgettext("collector-standard-bootstrap", "Paginated"),
                        value: "paginated",
                        description: pgettext(
                            "collector-standard-bootstrap",
                            "Blocks are presented page for page and the user navigates through the pages using the next and back buttons."
                        )
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Continuous"),
                        value: "continuous",
                        description: pgettext(
                            "collector-standard-bootstrap",
                            "This will keep all past blocks in view as the user navigates using the next and back buttons."
                        )
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Progressive"),
                        value: "progressive",
                        description: pgettext(
                            "collector-standard-bootstrap",
                            "In this mode all possible blocks are presented to the user. The user does not need to navigate using the next and back buttons (so we can hide those buttons)."
                        )
                    }
                ],
                default: "paginated"
            },
            {
                name: "optionsStatic",
                type: "static",
                title: pgettext("collector-standard-bootstrap", "Options")
            },
            {
                name: "showEnumerators",
                type: "checkbox",
                title: pgettext("collector-standard-bootstrap", "Display enumerators"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Specifies if block enumerators (question numbers) should be displayed."
                )
            },
            {
                name: "showPageIndicators",
                type: "checkbox",
                title: pgettext("collector-standard-bootstrap", "Dispay page indicators"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Shows an index with pages (only works in paginated mode when there is more than 1 page)."
                )
            },
            {
                name: "showProgressbar",
                dependsOn: "showNavigation",
                type: "checkbox",
                title: pgettext("collector-standard-bootstrap", "Display progressbar"),
                description: pgettext("collector-standard-bootstrap", "Specifies if the progressbar should be displayed.")
            }
        ]
    },
    {
        base: "backgroundImage",
        optional: true,
        title: pgettext("collector-standard-bootstrap", "Background image"),
        description: pgettext("collector-standard-bootstrap", "Specifies an optional background image."),
        styles: [
            {
                name: "url",
                type: "image",
                title: pgettext("collector-standard-bootstrap", "URL"),
                description: pgettext("collector-standard-bootstrap", "Specifies the URL of the background image.")
            },
            {
                name: "size",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Size"),
                description: pgettext("collector-standard-bootstrap", "Specifies the sizing of the background image."),
                options: [
                    {
                        label: pgettext("collector-standard-bootstrap", "Auto (image centered)"),
                        value: "auto"
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Width (100% width, height centered)"),
                        value: "100% auto"
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Height (width centered, 100% height)"),
                        value: "auto 100%"
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Cover (full screen)"),
                        value: "cover"
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Contain (full image stretched over screen)"),
                        value: "contain"
                    },
                    {
                        label: pgettext("collector-standard-bootstrap", "Repeat (repeat image over screen)"),
                        value: "repeat"
                    }
                ],
                default: "auto"
            }
        ]
    },
    {
        base: "form",
        optional: true,
        title: pgettext("collector-standard-bootstrap", "Form"),
        description: pgettext("collector-standard-bootstrap", "Specifies the appearance of the elements in the form."),
        styles: [
            {
                name: "inputStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Common elements"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Specifies the style of common elements (text inputs, dropdowns, etc.)."
                ),
                options: [
                    {
                        label: pgettext("collector-standard-bootstrap", "Default"),
                        value: ""
                    },
                    ...BOOTSTRAP_STYLES_BASIC
                ],
                default: ""
            },
            {
                name: "unselectedStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Unselected form buttons"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of form buttons that are not selected."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "light"
            },
            {
                name: "selectedStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Selected form buttons"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of form buttons that are selected."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "success"
            },
            {
                name: "neutralStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Neutral form buttons"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Specifies the style of form buttons that have a neutral value (e.g. default in Yes/No)."
                ),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "outline-secondary"
            },
            {
                name: "positiveStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Positive form buttons"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Specifies the style of form buttons that have a positive value (e.g. Yes in Yes/No)."
                ),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "success"
            },
            {
                name: "negativeStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Negative form buttons"),
                description: pgettext(
                    "collector-standard-bootstrap",
                    "Specifies the style of form buttons that have a negative value (e.g. No in Yes/No)."
                ),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "danger"
            }
        ]
    },
    {
        base: "buttons",
        optional: true,
        title: pgettext("collector-standard-bootstrap", "Buttons"),
        description: pgettext("collector-standard-bootstrap", "Specifies the appearance of the buttons in the form."),
        styles: [
            {
                name: "nextStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Next button color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the next button in the form."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "primary"
            },
            {
                name: "nextLabel",
                type: "label",
                title: pgettext("collector-standard-bootstrap", "Next button label"),
                description: pgettext("collector-standard-bootstrap", "Specifies the label of the next button in the form.")
            },
            {
                name: "backStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Back button color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the back button in the form."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "primary"
            },
            {
                name: "backLabel",
                type: "label",
                title: pgettext("collector-standard-bootstrap", "Back button label"),
                description: pgettext("collector-standard-bootstrap", "Specifies the label of the back button in the form.")
            },
            {
                name: "completeStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Complete button color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the complete button in the form."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "success"
            },
            {
                name: "completeLabel",
                type: "label",
                title: pgettext("collector-standard-bootstrap", "Complete button label"),
                description: pgettext("collector-standard-bootstrap", "Specifies the label of the complete button in the form.")
            },
            {
                name: "pauseStyle",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Pause button color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the pause button in the form."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "success"
            }
        ]
    },
    {
        base: "navigation",
        optional: true,
        dependsOn: "showNavigation",
        title: pgettext("collector-standard-bootstrap", "Navigation bar"),
        description: pgettext("collector-standard-bootstrap", "Specifies the appearance of the navigation bar."),
        styles: [
            {
                name: "paginationStyle",
                dependsOn: "showPageIndicators",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Pagination buttons"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the buttons in the pagination bar."),
                options: BOOTSTRAP_STYLES_EXTENDED,
                default: "light"
            },
            {
                name: "progressbarStyle",
                dependsOn: "showProgressbar",
                type: "dropdown",
                title: pgettext("collector-standard-bootstrap", "Progress bar color"),
                description: pgettext("collector-standard-bootstrap", "Specifies the style of the progress in the progress bar."),
                options: BOOTSTRAP_STYLES_BASIC,
                default: "primary"
            }
        ]
    }
];
