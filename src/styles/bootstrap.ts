import { pgettext } from "tripetto";

export const BOOTSTRAP_STYLES_BASIC = [
    {
        label: pgettext("collector-standard-bootstrap", "Primary"),
        value: "primary"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Secondary"),
        value: "secondary"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Success"),
        value: "success"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Info"),
        value: "info"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Warning"),
        value: "warning"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Danger"),
        value: "danger"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Light"),
        value: "light"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Dark"),
        value: "dark"
    }
];

export const BOOTSTRAP_STYLES_EXTENDED = [
    ...BOOTSTRAP_STYLES_BASIC,
    {
        label: pgettext("collector-standard-bootstrap", "Outline primary"),
        value: "outline-primary"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline secondary"),
        value: "outline-secondary"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline success"),
        value: "outline-success"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline info"),
        value: "outline-info"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline warning"),
        value: "outline-warning"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline danger"),
        value: "outline-danger"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline light"),
        value: "outline-light"
    },
    {
        label: pgettext("collector-standard-bootstrap", "Outline dark"),
        value: "outline-dark"
    }
];
