![Tripetto](https://docs.tripetto.com/assets/header.svg)

Tripetto is a full-fledged form kit. Rapidly create and deploy smart flowing forms and surveys. Drop the kit in your codebase and use all of it, or just the parts you need. The visual [**editor**](https://www.npmjs.com/package/tripetto) is for form creation, the [**collector**](https://www.npmjs.com/package/tripetto-collector) for response collection and the [**SDK**](https://docs.tripetto.com/guide/blocks) for developing more form building blocks. Does this sound too technical? Try the [**app**](https://tripetto.app)!

# Standard Bootstrap collector
[![Version](https://img.shields.io/npm/v/tripetto-collector-standard-bootstrap.svg)](https://www.npmjs.com/package/tripetto-collector-standard-bootstrap)
[![License](https://img.shields.io/npm/l/tripetto-collector-standard-bootstrap.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-collector-standard-bootstrap.svg)](https://www.npmjs.com/package/tripetto-collector-standard-bootstrap)
[![Pipeline status](https://gitlab.com/tripetto/collectors/standard-bootstrap/badges/master/pipeline.svg)](https://gitlab.com/tripetto/collectors/standard-bootstrap/commits/master)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/tripetto)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

This collector package is a graphical UI for Tripetto that implements a classic representation of a form using [Bootstrap](http://getbootstrap.com/) and [React](https://reactjs.org/).

# Demo
[![Try demo on CodePen](https://docs.tripetto.com/assets/button-demo.svg)](https://codepen.io/tripetto/pen/PMJzwZ)
[![View the code](https://docs.tripetto.com/assets/button-code.svg)](https://gitlab.com/tripetto/collectors/standard-bootstrap)

# Get started
There a multiple options how you can use this collector. From plain old HTML to [React](https://reactjs.org/) or using imports.

## Option A: Embed in HTML using CDN
```html
<script src="https://unpkg.com/tripetto-collector"></script>
<script src="https://unpkg.com/tripetto-collector-standard-bootstrap"></script>
<script>
TripettoCollectorStandardBootstrap.run({
    element: document.body,
    definition: /** Supply a form definition here. */,
    onFinish: function(instance) {
        /** Implement your response handler here. */
    }
});
</script>
```
[![Try demo on CodePen](https://docs.tripetto.com/assets/button-codepen.svg)](https://codepen.io/tripetto/pen/PMJzwZ)

## Option B: Using React
1. Install the required packages from npm:
```bash
$ npm install tripetto-collector
$ npm install tripetto-collector-standard-bootstrap
```
2. Use the React component:
```typescript
import { Collector } from "tripetto-collector-standard-bootstrap";

ReactDOM.render(
  <Collector
    definition={/** Supply a form definition here. */}
    onFinish={(instance: Collector.Instance) => {
      /** Implement your response handler here. */
    }}
    />,
  document.getElementById("root")
);
```
[![View the code](https://docs.tripetto.com/assets/button-codepen.svg)](https://codepen.io/tripetto/pen/mZadEX)

## Option C: Import from npm
1. Install the required packages from npm:
```bash
$ npm install tripetto-collector
$ npm install tripetto-collector-standard-bootstrap
```
2. Import the rolling collector:
```typescript
import * as Collector from "tripetto-collector";
import * as CollectorStandardBootstrap from "tripetto-collector-standard-bootstrap";

CollectorStandardBootstrap.run({
    element: document.body,
    definition: /** Supply a form definition here. */,
    onFinish: (instance: Collector.Instance) => {
        /** Implement your response handler here. */
    }
});
```

# Documentation
The complete Tripetto documentation can be found at [docs.tripetto.com](https://docs.tripetto.com).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/collectors/standard-bootstrap/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
