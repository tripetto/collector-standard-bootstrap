const webpack = require("webpack");
const webpackTerser = require("terser-webpack-plugin");
const path = require("path");
const package = require("./package.json");
const production = process.argv.includes("production");

const config = (dest, target) => {
    let entry;
    let output;
    let externals = {
        tripetto: target === "umd" ? "Tripetto" : "commonjs tripetto",
        "tripetto-collector": target === "umd" ? "TripettoCollector" : "commonjs tripetto-collector"
    };

    switch (dest) {
        case "collector":
            entry = "./src/index.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "collector", target),
                library: "TripettoCollectorStandardBootstrap",
                libraryTarget: target === "umd" ? "umd" : "commonjs2",
                umdNamedDefine: true
            };
            break;
        case "editor":
            entry = "./src/editor.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "editor", target),
                library: "TripettoEditorStandardBootstrap",
                libraryTarget: target === "umd" ? "umd" : "commonjs2",
                umdNamedDefine: true
            };
            break;
        default:
            entry = "./src/tests/app/app.tsx";
            output = {
                filename: "bundle.js",
                path: path.resolve(__dirname, "src/tests/app/static")
            };
            externals = [];
            break;
    }

    return {
        entry,
        output,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: "ts-loader",
                    options: {
                        compilerOptions: {
                            noEmit: false,
                            target: target === "es6" ? "ES6" : "ES5",
                            module: target === "es6" ? "es6" : "commonjs"
                        }
                    }
                },
                {
                    test: /\.svg$/,
                    use: ["url-loader"]
                },
                ...(dest !== "editor"
                    ? [
                          {
                              test: /\.(scss)$/,
                              use: [
                                  {
                                      loader: "style-loader"
                                  },
                                  {
                                      loader: "css-loader"
                                  },
                                  {
                                      loader: "postcss-loader",
                                      options: {
                                          plugins: function() {
                                              return [require("precss"), require("autoprefixer")];
                                          }
                                      }
                                  },
                                  {
                                      loader: "sass-loader"
                                  }
                              ]
                          },
                          {
                              test: /\.(woff|woff2|eot|ttf)$/,
                              use: [
                                  {
                                      loader: "url-loader"
                                  }
                              ]
                          }
                      ]
                    : [])
            ]
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js"]
        },
        externals,
        performance: {
            hints: false
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        output: {
                            comments: false
                        }
                    },
                    extractComments: false
                }),
                new webpack.BannerPlugin(`${package.title} ${package.version}`)
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                Promise: "es6-promise-promise"
            })
        ],
        devServer: {
            contentBase: path.join(__dirname, "src/tests/app/static"),
            port: 9000,
            host: "0.0.0.0"
        }
    };
};

module.exports = production
    ? [
          config("editor", "umd"),
          config("editor", "es5"),
          config("editor", "es6"),
          config("collector", "umd"),
          config("collector", "es5"),
          config("collector", "es6")
      ]
    : [config("app")];
